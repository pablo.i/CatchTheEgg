
extends RigidBody2D

var randomAccel = rand_range(-150.0,150.0)
onready var raycast_ball = get_node("RayCast2D")

func _ready():
	set_fixed_process(true)
	raycast_ball.add_exception(self)
	set_linear_velocity(Vector2(randomAccel,get_linear_velocity().y))

func collideWith(ball):
	print(raycast_ball.get_collider())

func _fixed_process(delta):
	if raycast_ball.is_colliding():
		raycast_ball.get_collider().collideWith(self)