
extends Node2D

var score = 0

func _ready():
	set_fixed_process(true)

func add_score():
	score = score + 1
	
func _fixed_process(delta):
	get_node("Label").set_text("SCORE: " + str(score * 10))