
extends KinematicBody2D

var btn_right = Input.is_action_pressed("button_right")
var btn_left = Input.is_action_pressed("button_left")
var player_speed = 15
var raycasti = null
var raycastd = null
var raycastc = null
var eggcount = 0

func collideWith(ball):
	eggcount += 1
	get_parent().add_score()
	get_node("player_sprite").get_node("AnimationPlayer").play("catch_egg")
	ball.queue_free()

func _ready():
	raycasti = get_node("RayCast2D")
	raycastd = get_node("RayCast2D 2")
	raycastc = get_node("RayCast2D 3")
	set_fixed_process(true)

func _fixed_process(delta):
	print(eggcount)
	raycasti.add_exception(self)
	raycastd.add_exception(self)
	raycastc.add_exception(self)
	btn_right = Input.is_action_pressed("button_right")
	btn_left = Input.is_action_pressed("button_left")
	if btn_right:
		move(Vector2(player_speed,0))
	if btn_left:
		move(Vector2(-player_speed,0))