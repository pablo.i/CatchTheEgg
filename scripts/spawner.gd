
extends Node2D

var ballScene = load("ball.tscn")
onready var myTimer = get_node("Timer")
var randomTime = rand_range(1.0,5.0)

func _ready():
	set_fixed_process(true)
	myTimer.set_autostart(true)

func _fixed_process(delta):
	myTimer.set_wait_time(randomTime)

func _on_Timer_timeout():
	var ballInstance = ballScene.instance()
	ballInstance.set_pos(get_pos())
	get_parent().add_child(ballInstance)
	randomTime = rand_range(0.1,1.0)